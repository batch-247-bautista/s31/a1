// 1. What directive is used by Node.js in loading the modules it needs?

      // Answer: "require" directive

// 2. What Node.js module contains a method for server creation?

      // Answer: http module
  

// 3. What is the method of the http object responsible for creating a server using Node.js?

      // Answer: createServer() method
  

// 4. What method of the response object allows us to set status codes and content types?
	
       // Answer: writeHead() method


// 5. Where will console.log() output its contents when run in Node.js?

       // Answer: web browser
       

// 6. What property of the request object contains the address's endpoint?

       // Answer: request


const http = require('http')

const port = 3000

const server = http.createServer((request, response) => {

      if (request.url == '/greetings'){

            response.writeHead(200, {'Content-Type': 'text/plain'})
            response.end('Welcome to the Multiverse!')
            console.log('Success!')

      } else if (request.url == '/login'){

            response.writeHead(200, {'Content-Type': 'text/plain'})
            response.end('Welcome to the login page!')
      
      } else {

            response.writeHead(404, {'Content-Type': 'text/plain'})
            response.end('Login Error')
      }
})

server.listen(port);

console.log(`Server is successfully running:${port}.`)
